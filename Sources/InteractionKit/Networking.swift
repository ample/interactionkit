//
//  Networking.swift
//  
//
//  Created by ben on 6/16/21.
//

import Foundation

internal struct Networking {
    
    static func log(data:Data, callback:((Response,String?) -> Void)?) {
        let urlStr = Settings.getString(key: .LogUrl)

        if(urlStr.isEmpty) {
            Logger.debug(Strings.SetLogUrl)
            callback?(.MissingUrl,Strings.SetLogUrl.rawValue)
            return
        }

        // create post request
        guard let url = URL(string: urlStr) else {
            Logger.debug(Strings.InvalidUrl)
            callback?(.Error,Strings.InvalidUrl.rawValue)
            return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = data

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            Logger.debug(Strings.Response, String(data:data ?? Data(),encoding:.utf8))
            
            guard let data = data, error == nil else {
                let err = error?.localizedDescription ?? Strings.NoData.rawValue
                Logger.debug(Strings.Error,err)
                callback?(.Error,err)
                return
            }
            
            let responseJSON = try? JSONSerialization.jsonObject(with: data, options: [])
            if let responseJSON = responseJSON as? [String: Any] {
                Logger.debug(Strings.Response, responseJSON)
                callback?(.Success,nil)
            } else {
                callback?(.Error,Strings.FailedToParseData.rawValue)
            }
        }

        task.resume()
    }
}
