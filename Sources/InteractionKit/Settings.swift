//
//  Settings.swift
//  
//
//  Created by ben on 6/16/21.
//

import Foundation

struct Settings {
    
    enum Key : String {
        case LogUrl = "logurl"
        case Debug = "debug"
    }
    
    static let suitename = "EventLogger"
    
    static func save(key:Key,value:Codable) {
        UserDefaults.init(suiteName: suitename)?.setValue(value, forKey: key.rawValue)
    }
    
    static func getString(key:Key) -> String {
        return UserDefaults.init(suiteName: suitename)?.string(forKey: key.rawValue) ?? ""
    }
    
    static func getBool(key:Key) -> Bool {
        return UserDefaults.init(suiteName: suitename)?.bool(forKey: key.rawValue) ?? false
    }
    
}
