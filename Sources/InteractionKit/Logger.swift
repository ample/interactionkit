//
//  Logger.swift
//  
//
//  Created by ben on 6/16/21.
//

import Foundation

struct Logger {
    
    static func log(_ str:Strings) {
        print(str.rawValue)
    }
    
    static func log(_ str:String) {
        print(str)
    }
    
    static func debug(_ str:Strings, _ extra:Any? = nil) {
        let debug = Settings.getBool(key: .Debug)
        if(debug) {
            print("\(Strings.Prefix.rawValue) \(str.rawValue) \(extra ?? "") \(Strings.Suffix.rawValue)")
        }
    }
}
