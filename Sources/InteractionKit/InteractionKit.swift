public struct InteractionKit {
    
    public static func register(url:String) {
        Settings.save(key: .LogUrl, value: url)
    }
    
    public static func debug(val:Bool) {
        Settings.save(key: .Debug, value: val)
    }
    
    public static func log(eventName:String,action:Action,attribute:String? = nil,userId:Int? = nil, callback: ((Response,String?) -> Void)?) {
        var postParams = [
            NetworkParameter(key: "event", value: eventName)
        ]
        
        if let userId = userId {
            postParams.append(NetworkParameter(key: "userId", value: "\(userId)"))
        }
        
        if let attribute = attribute {
            postParams.append(NetworkParameter(key: "attribute", value: attribute))
        }
        
        guard let json = Encoder.encode(params: postParams) else {
            callback?(.Error,"Failed to encode json")
            return
        }
        
        Networking.log(data:json ,callback:callback)
    }
    
    public static func log(params:[NetworkParameter], callback: ((Response,String?) -> Void)?) {
        var dict:[String:String] = [:]
        params.forEach { (param) in
            dict[param.key] = param.value
        }
        
        return log(dict: dict, callback: callback)
    }
    
    public static func log(dict:[String:String], callback: ((Response,String?) -> Void)?) {
        guard let json = Encoder.encode(dict: dict) else {
            callback?(.Error,"Failed to encode json")
            return
        }
        
        Networking.log(data:json ,callback:callback)
    }
}
