//
//  NetworkParameter.swift
//  
//
//  Created by ben on 6/16/21.
//

import Foundation

public struct NetworkParameter : Codable {
    var key:String
    var value:String
}
