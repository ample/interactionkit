//
//  File.swift
//  
//
//  Created by ben on 6/16/21.
//

import Foundation

public enum Response {
    case Success
    case MissingUrl
    case Error
}
