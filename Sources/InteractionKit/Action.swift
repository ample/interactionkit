//
//  File.swift
//  
//
//  Created by ben on 6/16/21.
//

import Foundation

public enum Action : String {
    case View = "View"
    case Click = "Click"
    case Tap = "Tap"
    case Swipe = "Swipe"
}
