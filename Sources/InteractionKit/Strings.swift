//
//  File.swift
//  
//
//  Created by ben on 6/16/21.
//

import Foundation

enum Strings : String {
    case Prefix = "----InteractionKit:"
    case Suffix = "----"
    case JsonEncodeError = "Failed to encode JSON"
    case FailedToParseData = "Failed to parse data"
    case SetLogUrl = "Set your log url before logging an event"
    case InvalidUrl = "URL is not valid"
    case Error = "Error"
    case Response = "Response"
    case EncodedData = "Encoded Data:"
    case NoData = "No Data"
}
