//
//  Encoder.swift
//  
//
//  Created by ben on 6/16/21.
//

import Foundation

internal struct Encoder {
    internal static func encode(params:[NetworkParameter]) -> Data? {
        var dict = [String:String]()
        params.forEach { (param) in
            dict[param.key] = param.value
        }
        return encode(dict: dict)
    }
        
    internal static func encode(dict:[String:String]) -> Data? {
        
        var jsonData:Data? = nil
        do {
            jsonData = try JSONEncoder().encode(dict)
            let dataAsString = String(data:jsonData ?? Data(), encoding:.utf8)
            Logger.debug(Strings.EncodedData,dataAsString)
            return jsonData
        } catch {
            return nil
        }
    }
}
