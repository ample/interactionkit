    import XCTest
    @testable import InteractionKit

    final class InteractionKitTests: XCTestCase {
        func testEmptyUrl() throws {
            InteractionKit.register(url: "")
            InteractionKit.log(eventName: "Something",action: .Click,attribute: "firstClick",userId: 1) { res, err in
                XCTAssert(res == .MissingUrl)
            }
        }
        
        func testInvalidUrl() throws {
            InteractionKit.register(url: "htt//google.com")
            InteractionKit.log(eventName: "Something",action: .Click,attribute: "firstClick",userId: 1) { res, err in
                XCTAssert(res == .Error)
            }
        }
        
        func testSuccess() throws {
            var didExecute = false
            InteractionKit.register(url: "https://amplesftwr.com/basicJson.php")
            InteractionKit.log(eventName: "Something",action: .Click,attribute: "firstClick",userId: 1) { res, err in
                XCTAssert(res == .Success)
                didExecute = true
            }
            sleep(3)
            XCTAssert(didExecute)
        }
    }
